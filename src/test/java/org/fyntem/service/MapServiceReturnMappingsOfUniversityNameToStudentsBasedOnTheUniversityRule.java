package org.fyntem.service;

import org.fyntem.entity.GradeLevel;
import org.fyntem.entity.Student;
import org.fyntem.entity.University;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class MapServiceReturnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule {

    @Test
    public void testReturnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule1() {
        List<University> universities = List.of(
                new University("MIT", GradeLevel.A),
                new University("KPI", GradeLevel.D),
                new University("Toulouse", GradeLevel.B),
                new University("Harvard", GradeLevel.A)
        );
        List<Student> students = List.of(
                new Student("Johnson", 65),
                new Student("Thatcher", 97),
                new Student("Li", 98),
                new Student("Tyler", 85),
                new Student("Xiaoyu", 75),
                new Student("White", 59)
        );

        final Map<String, List<Student>> resultMap = MapService.returnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule(students, universities);

        assert resultMap != null;

        assertTrue(resultMap.containsKey("MIT"));
        assertTrue(resultMap.containsKey("KPI"));
        assertTrue(resultMap.containsKey("Toulouse"));
        assertTrue(resultMap.containsKey("Harvard"));

        assertIterableEquals(List.of(new Student("Thatcher", 97), new Student("Li", 98)), resultMap.get("MIT"));
        assertIterableEquals(List.of(
                new Student("Thatcher", 97),
                new Student("Li", 98),
                new Student("Tyler", 85),
                new Student("Xiaoyu", 75)), resultMap.get("KPI"));
        assertIterableEquals(List.of(
                new Student("Thatcher", 97),
                new Student("Li", 98),
                new Student("Tyler", 85)), resultMap.get("Toulouse"));
        assertIterableEquals(List.of(new Student("Thatcher", 97), new Student("Li", 98)), resultMap.get("Harvard"));
    }

    @Test
    public void testReturnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule2() {
        assertThrows(
                IllegalArgumentException.class,
                () -> MapService.returnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule(Collections.emptyList(), List.of(new University("Harvard", GradeLevel.A))),
                "Students list should exist and not be empty");
    }

    @Test
    public void testReturnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule3() {
        assertThrows(
                IllegalArgumentException.class,
                () -> MapService.returnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule(List.of(new Student("Jackson", 75)), Collections.emptyList()),
                "Students list should exist and not be empty");
    }

    @Test
    public void testReturnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule4() {
        List<University> universities = List.of(
                new University("Oxford", GradeLevel.A),
                new University("Cambridge", GradeLevel.B),
                new University("KNTU", GradeLevel.E)
        );
        List<Student> students = List.of(
                new Student("Johnson", 65),
                new Student("Thatcher", 97),
                new Student("Li", 98),
                new Student("Tyler", 85),
                new Student("Xiaoyu", 75),
                new Student("White", 59)
        );

        final Map<String, List<Student>> resultMap = MapService.returnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule(students, universities);

        assert resultMap != null;

        assertTrue(resultMap.containsKey("Oxford"));
        assertTrue(resultMap.containsKey("Cambridge"));
        assertTrue(resultMap.containsKey("KNTU"));

        assertIterableEquals(List.of(new Student("Thatcher", 97), new Student("Li", 98)), resultMap.get("Oxford"));
        assertIterableEquals(List.of(
                new Student("Thatcher", 97),
                new Student("Li", 98),
                new Student("Tyler", 85)), resultMap.get("Cambridge"));
        assertIterableEquals(List.of(
                new Student("Johnson", 65),
                new Student("Thatcher", 97),
                new Student("Li", 98),
                new Student("Tyler", 85),
                new Student("Xiaoyu", 75)), resultMap.get("KNTU"));
    }

}
