package org.fyntem.service;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class collectionServiceReturnAllNonNullDuplicationsFromAList {

    @Test
    public void testReturnReturnAllNonNullDuplicationsFromAList1() {
        List<String> wordList = List.of("Sometimes", "we", "use", "", "the", "time", "the", "", "different", "way", "we", "expect");
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueWordsFromAList(wordList);

        assertIterableEquals(new LinkedHashSet<>(List.of("Sometimes", "we", "use", "the", "time", "different", "way", "expect")), resultSet);
    }

    @Test
    public void testReturnReturnAllNonNullDuplicationsFromAList2() {
        List<String> wordList = new ArrayList<>();
        wordList.add("");
        wordList.add(null);
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueWordsFromAList(wordList);

        assertNull(resultSet);
    }


    @Test
    public void testReturnAllNonNullDuplicationsFromAListLambda1() {
        List<String> wordList = List.of("Sometimes", "we", "use", "", "the", "time", "the", "", "different", "way", "we", "expect");
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueFromAListLambda(wordList);

        Set<String> expectedSet = new LinkedHashSet<>(List.of("Sometimes", "we", "use", "the", "time", "different", "way", "expect"));
        assertEquals(expectedSet, resultSet);
    }

    @Test
    public void testReturnAllNonNullDuplicationsFromAListLambda2() {
        List<String> wordList = new ArrayList<>();
        wordList.add("");
        wordList.add(null);
        Set<String> resultSet = CollectionService.returnAllNonNullUniqueFromAListLambda(wordList);

        assertNull(resultSet);
    }


}
