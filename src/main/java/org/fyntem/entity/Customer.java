package org.fyntem.entity;

import java.util.Objects;

public class Customer {
    private int queueNumber;
    private String customerName;

    public Customer(int queueNumber, String customerName) {
        this.queueNumber = queueNumber;
        this.customerName = customerName;
    }

    public int getQueueNumber() {
        return queueNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return queueNumber == customer.queueNumber && Objects.equals(customerName, customer.customerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queueNumber, customerName);
    }
}