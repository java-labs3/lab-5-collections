package org.fyntem.entity;

import java.util.Objects;

public class Student {
    private String surname;
    private int averageGrade; // range [0 - 100]

    public Student(String surname, int averageGrade) {
        this.surname = surname;
        this.averageGrade = averageGrade;
    }

    public String getSurname() {
        return surname;
    }

    public int getAverageGrade() {
        return averageGrade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return averageGrade == student.averageGrade && Objects.equals(surname, student.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surname, averageGrade);
    }

}
