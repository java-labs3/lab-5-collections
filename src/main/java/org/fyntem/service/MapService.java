package org.fyntem.service;

import org.fyntem.entity.Student;
import org.fyntem.entity.University;
import org.fyntem.entity.GradeLevel;

import java.util.*;

public class MapService {

    /**
     * Given the List of Students and Universities.
     *
     * @return mapping of University name to Students that are allowed to enter that University.
     * <p>
     * Requirements:
     * 1. be able to guess, why the University name, instead of plain University object were used in returning map;
     * 2. explain how a HashMap works.
     */
    public static Map<String, List<Student>> returnMappingsOfUniversityNameToStudentsBasedOnTheUniversityRule(List<Student> students, List<University> universities) {
        if (students == null || students.isEmpty()) {
            throw new IllegalArgumentException("Students list should exist and not be empty");
        }
        if (universities == null || universities.isEmpty()) {
            throw new IllegalArgumentException("Universities list should exist and not be empty");
        }

        Map<String, List<Student>> result = new HashMap<>();

        for (University university : universities) {
            List<Student> acceptedStudents = new ArrayList<>();
            for (Student student : students) {
                if (student.getAverageGrade() >= university.getLowerGradeAcceptingFrom().getGradeStartsFrom()) {
                    acceptedStudents.add(student);
                }
            }
            result.put(university.getName(), acceptedStudents);
        }

        return result;
    }
}
