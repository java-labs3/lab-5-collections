package org.fyntem.service;

import org.fyntem.entity.Customer;
import org.fyntem.entity.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;

public class CollectionService {


    /**
     * Given a List of elements, N - symbolsLimit
     * Using for-cycle
     *
     * @return all non-null/non-empty words that have equal or less than N symbols.
     */
    public static List<String> returnAllNonNullBiggerThanNSymbolsFromAList(List<String> stringList, int symbolsLimit) {
        if (symbolsLimit < 0) {
            throw new IllegalArgumentException("Limit must be non-negative");
        }

       if (symbolsLimit == 0) {
            return List.of("");
        }

        List<String> result = new ArrayList<>();

        for (String s : stringList) {
            if (s != null && !s.trim().isEmpty() && s.trim().length() <= symbolsLimit) {
                result.add(s.trim());
            }
        }
        return result;

    }


    /**
     * Given a List of elements, N - symbolsLimit
     * Using lambda-expressions
     *
     * @return all non-null/non-empty words that have less than N symbols.
     */
    public static List<String> returnAllNonNullBiggerThanNSymbolsFromAListLambda(List<String> stringList, int symbolsLimit) {
        if (symbolsLimit < 0) {
            throw new IllegalArgumentException("Limit must be non-negative");
        }
        if (symbolsLimit == 0) {
            return List.of("");
        }

        return stringList.stream().filter(s -> s != null && !s.trim().isEmpty() && s.trim().length() <= symbolsLimit).collect(Collectors.toList());

    }


    /**
     * Given a List of elements and a number N.
     * Take N elements from the end of a list and append them to it's beginning.
     * <p>
     * Example: ["one", "two", "three", "four", "five"], N = 3
     * return ["three", "four", "five", "one", "two"]
     *
     * @return new list.
     */
    public static List<String> putNElementsToTheBeginning(List<String> stringList, int n) {
        if (n < 0) {
            throw new IllegalArgumentException("N must be non-negative");
        }

        final LinkedList<String> strings = new LinkedList<>(stringList);
        for (int i = 0; i < n; i++) {
            strings.push(strings.pollLast());
        }
        return strings;
    }


    /**
     * Given a List of Customers and a day of week.
     *
     * @return queue of the names of all Customers based on their queue number and
     * add restrict them to the operational day
     * <p>
     * Example: [{"Mike", 6}, {"John", 3}, {"Garry", 1}, {"Ian", 7}, {"Andy", 2}, {"Liz", 4}, {"Fred", 5}], DayOfWeek = MONDAY
     * return [{"Garry", 1}, {"Andy", 2}, {"John", 3}, {"Liz", 4}, {"Fred", 5}]
     * <p>
     * Requirement:
     * Select the most fit Queue Interface implementation and explain why you did so.
     * Also be ready to explain: "can we use something better than queue here than a queue? If so - in which cases?"
     */


    public static Queue<Customer> returnAValidQueueBasedOnCustomerNumber(List<Customer> customers, DayOfWeek dayOfWeek) {
        Deque<Customer> deque = new ArrayDeque<>();

        for (Customer customer : customers) {
            if (customer.getQueueNumber() <= dayOfWeek.getQueueLimit()) {
                deque.offer(customer);
            }
        }

        List<Customer> validCustomers = new ArrayList<>(deque);
        validCustomers.sort(Comparator.comparingInt(Customer::getQueueNumber));

        deque = new ArrayDeque<>(validCustomers);

        if (deque.isEmpty()) {
            throw new IllegalArgumentException("No customers are expected on " + dayOfWeek);
        }

        return deque;
    }


    /**
     * Given a List of elements.
     * Using for-cycle
     *
     * @return all non-null/non-empty duplications from a given list.
     */
    public static Set<String> returnAllNonNullUniqueWordsFromAList(List<String> wordList) {
        if (wordList == null) {
            return null;
        }
        Set<String> resultSet = new LinkedHashSet<>();
        for (String word : wordList) {
            if (word != null && !word.isEmpty()) {
                resultSet.add(word);
            }
        }
        if (resultSet.isEmpty()){
            return null;
        } else {
            return resultSet;
        }
    }


    /**
     * Given a List of elements.
     * Using lambda-expressions
     *
     * @return all non-null/non-empty duplications from a given list.
     */
    public static Set<String> returnAllNonNullUniqueFromAListLambda(List<String> wordList) {

        LinkedHashSet<String> resultSet = wordList.stream().filter(s -> s != null && !s.isBlank()).collect(Collectors.toCollection(LinkedHashSet::new));

      return resultSet.isEmpty() ? null : resultSet;

}
}

